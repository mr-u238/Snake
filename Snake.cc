#include <iostream>
#include <sys/ioctl.h>
#include <thread>
#include <unistd.h>
#include <string>
#include <fstream>
#include "nodo.h"
using namespace std;

#include <termios.h>

/*Dichiarazione costanti*/
const string BORDO = "#";
const string TESTA = "O";
const string CORPO = "o";
const string FRUTTO = "ò";
const int VELOCITÀ = 100;
const char DIREZIONE_INIZIALE='d';
const int ZERO = 0;
const int UNO = 1;
const int DUE = 2;

/*Utilizzato per rappresentare un elemento del serpente (coda o testa).
Per non fare confusione utilizzeremo il termine "nodo"*/
struct nodo{
	int Identificatore;
	char Posizione_attuale;
	char Posizione_precedente;
	nodo * seguente;
};

/*Coda (dato astratto) per gestire i vari nodi -- Non dovrebbe servire*/
/*struct lista {
	nodo nome;
	lista * seguente;
};*/

/*Definizione delle varie funzioni*/
void scene(int &line, int &column, int &yhead, int &xhead, int &xfrutto, 
	int &yfrutto, int &lunghezza, char &direzione
);

void sposta(int &xhead, int &yhead, char input, int velocità);

void tasto(char &dir);

//template<typename CharT>
//void dump(std::string_view s, const CharT c);

void controlla_bordo(int &line, int &column, int &yhead, int &xhead);

char getch(void);

/*Main*/
int main (int argc, char* argv[]){

	/* ottiene le impostazioni correnti del terminale */
	struct termios before;
	tcgetattr (STDIN_FILENO, &before);

	/* disabilita ECHO e ICANON */
	struct termios after = before;
	after.c_lflag &= ~(ECHO | ICANON);
	tcsetattr (STDIN_FILENO, TCSANOW, &after);
	
	using namespace std::this_thread;
    using namespace std::chrono;
	struct winsize w;
	int key = ZERO;
    ioctl(ZERO, TIOCGWINSZ, &w);
    /*Bordi del campo di gioco*/
    int line, column;
    line = w.ws_row - DUE;
    column = w.ws_col;
    int x = ZERO;
    //Posizione attuale
    int yhead = 10;
    int xhead = 10;
    //Posizione frutto
    int xfrutto = 15;
    int yfrutto = 15;
    int punteggio = ZERO;
    char input = DIREZIONE_INIZIALE;
    char direzione = DIREZIONE_INIZIALE;
    int velocità = 100;
    int lunghezza = ZERO;
    /*Crea il primo nodo (la testa)*/
    init();
    push(lunghezza, DIREZIONE_INIZIALE, xhead, yhead);
    while (x == ZERO){
    
    	//sleep_for(milliseconds(500));
    	
    	std::thread tastiera(tasto, std::ref(input));
    	tastiera.detach();
    	
    	/*Aumenta o riduce la velocità di movimento ('+' o '-')*/
    	if (input == '+'){
    		velocità--;
    	}else if (input == '-'){
    		velocità++;
    	}
    	
    	/*Cambia la direzione di movimento*/
    	if (input == 'w'){
    		if (direzione != 's'){
    			direzione = 'w';
    		}
    	} else if (input == 'd'){
    		if (direzione != 'a'){
    			direzione = 'd';
    		}
    	}else if (input == 's'){
    		if (direzione != 'w'){
    			direzione = 's';
    		}
    	}else if (input == 'a'){
    		if (direzione != 'd'){
    			direzione = 'a';
    		}
    	}
   		
   		/*Chiama la funzione per spostare il personaggio*/
   		//sposta(xhead, yhead, direzione, velocità);
   		
   		/*Chiama la funzione per controllare l'impatto con un bordo*/
   		controlla_bordo(line, column, yhead, xhead);
   		
   		/*Controlla se il frutto è stato mangiato e ne crea un altro*/
   		if (xhead != xfrutto){
   		} else {
   			if (yhead != yfrutto){
   			} else{
   				xfrutto = rand() % (line - DUE) + UNO;
   				yfrutto = rand() % (column - DUE) + UNO;
   				punteggio++;
   				lunghezza++;
   				push(lunghezza, direzione, ZERO, ZERO);
   			}
   		}
   		    
    	/*Pulisci il terminale*/
    	system("clear"); 
    	
    	/*Stampa la posizione --da rimuovere nella versione finale*/
    	cout << xhead << " " << yhead;
    	cout << " " << xfrutto << " " << yfrutto;
    	cout << " Punteggio: " << punteggio;
    	
    	/*Stampa l'area e il personaggio*/
		scene(line, column, yhead, xhead, xfrutto, yfrutto, lunghezza, direzione);
		
		/*Mette il gioco in pausa (In progresso)*/
		if (input == 'p'){
			cout << "PAUSA";
			char k = DIREZIONE_INIZIALE;
			input = direzione;
			while (k != 'p'){
				if (input == 'p'){
					k = 'p';
				}
			}
			input = direzione;
		}
		
		/*Chiude il gioco alla pressione del tasto q*/
		if (input == 'q'){
			cout << "Grazie per aver giocato!" << endl;
			
			/* ripristina le impostazioni predefinite del terminale */
			tcsetattr (STDIN_FILENO, TCSANOW, &before);
			
			sleep_for(milliseconds(500));
			
			/*Conclude il ciclo while, e quindi termina il programma*/
			x = UNO;
		}
    };
}

void scene(int &line, int &column, int &yhead, int &xhead, int &xfrutto, 
	int &yfrutto, int &lunghezza, char &direzione
){
	fstream scrivi;
	scrivi.open("log1.txt", ios::out|ios::app);
	//HANDLE hConsole = GetStdHandle(STD_OUTPUT_HANDLE);
	/*Genera il mondo in base alle dimensioni della finestra*/
	string matrix[line][column];
	/*Codice da iterare per inserire i vari nodi nella matrice (DAFARE!)*/
	int id, px, py;
	char da, dv; //dv = direzione vera
	scrivi << "Lunghezza: " << lunghezza << endl;
	for (int x = ZERO; x <= lunghezza; x++){
		scrivi << "Aggiungendo n." << x << endl;
	    recupera(id, da, px, py);
	    if (id == 0){
	    	dv = direzione;
	    } else {
	    	dv = da;
	    }
	    aggiorna(id, dv, px, py);
	    if (id != 0){
	    	matrix[px][py] = CORPO;
	    } else {
	    	matrix[px][py] = TESTA;
	    }
	    prossimo();
	}
	
	//Setting objects of the matrix empty (spaces)
	for (int x = ZERO; x < line; x++){
	    for (int y = ZERO; y < column; y++){
    		matrix[x][y] = ' ';
    	}
	}
	/*Crea il bordo superiore*/
	for(int x = ZERO; x < UNO; x++){
		for (int y = ZERO; y < column; y++){
    		matrix[x][y] = BORDO;
    	}
	}
	/*Crea il bordo inferiore*/
	for (int y = ZERO; y < column; y++){
   		matrix[line - UNO][y] = BORDO;
    }
	/*Crea il bordo destro*/
    for (int x = ZERO; x < line; x++){
    	matrix[x][ZERO] = BORDO;
    }
	/*Crea il bordo sinistro*/
    for (int x = ZERO; x < line; x++){
    	matrix[x][column - UNO] = BORDO;
    }
	/*Crea il personaggio*/
    matrix[xhead][yhead] = TESTA;
    /*Crea il frutto*/
    matrix[xfrutto][yfrutto] = FRUTTO;
    //Print the Matrix
    for (int x = ZERO; x < line; x++){
	    for (int y = ZERO; y < column; y++){
    		cout << matrix[x][y];
    	}
    	cout << endl;
	}
	scrivi << "Stampa conclusa" << endl;
	
}

/*Controlla se l'oggetto sta per uscire dal bordo e lo riporta dentro*/
void controlla_bordo(int &line, int &column, int &yhead, int &xhead){
   	if (xhead <= ZERO){
   		xhead = line - DUE;
   	}
   	if (xhead >= line - UNO){
   		xhead = UNO;
   	}
   	if (yhead <= ZERO){
   		yhead = column - DUE;
   	}
    if (yhead >= column - UNO){
   		yhead = UNO;
   	}
}

/*Sposta il personaggio*/
void sposta(int &xhead, int &yhead, char input, int velocità){
	std::this_thread::sleep_for(std::chrono::milliseconds(velocità));

	switch(input){
    		case 'w':
   				xhead--;
   				break;
   			case 's':
   				xhead++;
    			break;
    		case 'a':
    			yhead--;
    			break;
   			case 'd':
   				yhead++;
   				break;
   		}
}

/*Ottiene l'input dell'utente*/
void tasto(char &dir){
	dir = getchar();
}
