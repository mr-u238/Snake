#ifndef HEADER_H
#define HEADER_H
void init ();
void push (int, char, int, int);
void aggiorna (int &id, char &da, int &px, int &py);
void recupera (int &id, char &da, int &px, int &py);
void prossimo ();
#endif
