# Serpente

Implementazione per terminale dello storico gioco del serpente (Snake)

<h1>Compilazione</h1>

Per prima cosa assicurati di aver installato g++ 11.4.0 (versioni più recenti dovrebbero andare bene lo stesso)

    git clone --recursive [REPOSITORY URL]
    cd [Repository clonato]
    g++ Snake.cc nodo.cc
