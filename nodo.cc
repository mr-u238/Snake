// q è il primo elemnto, ovvero il primo ad uscire. t è l'elemento attuale.
#include <iostream>
#include <string>
#include <fstream>
using namespace std;
#include "nodo.h"

struct contatti{
	int identificatore;
	char direzione_attuale;
	int posizione_x;
	int posizione_y;
	contatti * seguente;
};
fstream scrivi;
contatti * t = new contatti;
contatti * q = new contatti;
contatti * r = new contatti;

void init (){
	q = NULL;
	t->seguente = NULL;
	scrivi.open("log.txt", ios::out|ios::in);
	scrivi << "Inizio" << endl;
}

void push (int id, char da, int px, int py) {
	contatti * nuovo = new contatti;
	nuovo->identificatore = id;
	nuovo->direzione_attuale = da;
	nuovo->posizione_x = px;
	nuovo->posizione_y = py;
	nuovo->seguente = NULL;
	if (q == NULL){
		nuovo->direzione_attuale = da;
		q = nuovo;
		q->seguente = NULL;
		t = q;
		cout << "Primo elemento";
		scrivi << "Generato elemento n." << nuovo->identificatore << " (nodo iniziale)" << endl;
	} else if (q->seguente == NULL){
		nuovo->direzione_attuale = t->direzione_attuale;
		t = nuovo;
		t->seguente = q->seguente;
		q->seguente = t->seguente;
		scrivi << "Generato elemento n." << nuovo->identificatore << endl;
	} else {
		nuovo->direzione_attuale = t-> direzione_attuale;
		t->seguente = nuovo;
		t = t->seguente;
		//t->seguente = q;
		scrivi << "Generato elemento n." << nuovo->identificatore << endl;
	}
}

/*-Muove la posizione e la direzione dell'oggetto attuale (q)*/
void aggiorna (int &id, char &da, int &px, int &py){
	switch(da){
    		case 'w':
   				t->posizione_x = px - 1;
   				break;
   			case 's':
   				t->posizione_x = px + 1;
    			break;
    		case 'a':
    			t->posizione_y = py - 1;
    			break;
   			case 'd':
   				t->posizione_y = py + 1;
   				break;
   		}
   	t->direzione_attuale = da;
   	px = t-> posizione_x;
   	py = t-> posizione_y;
   	scrivi << "Aggiornando" << endl;
}

/*Restituisce il nodo puntato (q)*/
void recupera (int &id, char &da, int &px, int &py){
	id = t->identificatore;
	da = t->direzione_attuale;
	px = t->posizione_x;
	py = t->posizione_y;
	scrivi << "Recuperando posizione n." << id << endl;
}

/*q punta al seguente elemento che dovrà essere utilizzato*/
void prossimo(){
	if (t->seguente == NULL){
		t = q;
		scrivi << "Raggiunta la fine, riportando il puntatore all'inizio" << endl;
	} else {
		int num = t->identificatore;
		t = t->seguente;
		scrivi << "Spostando puntatore da n." << num << " a n." << t->identificatore<< endl;
	}
}

